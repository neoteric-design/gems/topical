class CreateTopicalTags < ActiveRecord::Migration[5.0]
  def change
    create_table :topical_tags do |t|
      t.string :name
      t.string :slug, index: true, unique: true
      t.string :context

      t.timestamps null: false
    end

    create_table :topical_taggings do |t|
      t.references :taggable, index: true, polymorphic: true
      t.references :tag, index: true
      t.integer :position

      t.timestamps null: false
    end
  end
end
