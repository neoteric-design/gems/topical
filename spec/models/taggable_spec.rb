require 'rails_helper'

# Model in Dummy App

TaggableExample.class_eval do
  topical_on :species, :topics
  topical_on :ideas
end

RSpec.describe TaggableExample, type: :model do
  include Topical::TaggableSpec

  it "topical_on" do
    expected_contexts = %i[species topics ideas]
    expect(TaggableExample.topical_contexts).to match_array(expected_contexts)
  end

  describe "#tagged_on" do
    let(:taggable) { TaggableExample.new }
    it "takes a context and a name, returns tagged instances of that class" do
      taggable.send("#{a_context}_list=", "Feline,Canine")
      taggable.save
      expect(taggable.class.tagged_on(:species, 'Feline')).to include(taggable)
    end
  end

  describe "#tagged_with" do
    let(:taggable) { TaggableExample.new }
    it "takes a tag object, returns tagged instances of that class" do
      taggable.send("#{a_context}_list=", "Feline,Canine")
      tag = Topical::Tag.find_by(name: 'Feline', context: a_context)
      taggable.save
      expect(taggable.class.tagged_with(tag)).to include(taggable)
    end
  end
end
