require 'rails_helper'

module Topical
  RSpec.describe Tagging, type: :model do
    it { is_expected.to respond_to :taggable, :tag, :position }
  end
end
