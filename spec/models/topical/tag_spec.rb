require 'rails_helper'

module Topical
  RSpec.describe Tag, type: :model do
    it { is_expected.to respond_to :name, :slug, :context }
    it { is_expected.to respond_to :taggings, :taggables }

    describe "name" do
      let!(:existing)    { Topical::Tag.create(name: 'Cool', context: 'beans') }
      let(:collision)    { Topical::Tag.create(name: ' cool', context: 'beans') }
      let(:collision2)   { Topical::Tag.create(name: 'Cool', context: 'beans') }
      let(:diff_context) { Topical::Tag.create(name: 'Cool', context: 'jeans') }
      let(:diff_name)    { Topical::Tag.create(name: 'Fool', context: 'beans') }

      it "is unique within a context" do
        expect(collision.errors.keys).to include(:slug)
        expect(collision2.errors.keys).to include(:slug)
        expect(diff_name.errors).to be_empty
        expect(diff_context.errors).to be_empty
      end

      it 'is required' do
        empty_tag = Topical::Tag.create(name: '')
        space_tag = Topical::Tag.create(name: ' ')
        expect(empty_tag.errors.keys).to include(:name)
        expect(space_tag.errors.keys).to include(:name)
      end
    end

    describe "self.by" do
      %i(beans jeans spleens).each do |context|
        let!(context) { Topical::Tag.create(name: 'Cool', context: context.to_s) }
        scenario "accepts symbol" do
          expect(Topical::Tag.by(context)).to eq([send(context)])
        end

        scenario "accepts string" do
          expect(Topical::Tag.by(context.to_s)).to eq([send(context)])
        end
      end
    end

    describe "slug" do
      let!(:cool) { Topical::Tag.create(name: 'Cool', context: 'beans') }
      let!(:hot) { Topical::Tag.create(name: "Steamin' Hot", context: 'beans') }

      it "has a slug based on name" do
        expect(cool.slug).to eq('cool')
        expect(hot.slug).to  eq('steamin-hot')
      end
    end
  end
end
