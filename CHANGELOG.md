
0.4.0
=====

* Ruby 3 fixes
* Drop Rails 4.2 support

0.3.0
=====

* Use `delete_all` on taggings for fast delete
* Fix bug when calling `topical_on` multiple times per class
* Freeze string literals with magic comments to shave down memory usage
* Use `optional: true` instead of `required: false` on belong_to's per that
  quick deprecation

0.2.3
=====

* Fix issue with new default required: true behavior on belongs_to in Rails 5.1

0.2.2
=====

* Versionify migration for Rails 5.1

0.2.1
=====

* Fix bug in tag factory when list is nil

0.2.0
=====

* Assign tags from list in a more Rails idiosyncratic way

0.1.0
=====

* Initial release
