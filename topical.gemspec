$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'topical/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'topical'
  s.version     = Topical::VERSION
  s.authors     = ['Madeline Cowie']
  s.email       = ['madeline@cowie.me']
  s.homepage    = 'http://neotericdesign.com'
  s.summary     = 'Super simple tagging engine for ActiveRecord models'
  s.description = 'Developed for Neoteric Design, Inc.'

  s.files = Dir['{app,config,db,lib}/**/*', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['spec/**/*']

  s.add_dependency 'rails', '>= 5'

  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'sqlite3'
end
