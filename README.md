# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

= Topical
# Super simple tagging engine for ActiveRecord models

## Getting started

`rails g topical:install`
`rake db:migrate`

## Usage
```
class YourModel < ActiveRecord::Base
  topical_on :topics, :colors, :things
end
```
YourModel.tagged_with(:colors, 'Blue')
=> []




Instances of YourModel will now have AR relations for each topic kind (context) you set.

### Working with forms
For convenience, provided are a special _list getter and setter for each context.

Using the above example

```
thing = YourModel.new
thing.colors_list = "Blue,Yellow,Green"
thing.save

thing.colors
=> [Topical::Tag(name: "Blue", context: "colors"), Topical::Tag(name: "Yellow", context: "colors") ...
thing.colors_list
=> "Blue,Yellow,Green"
```

You can use these in conjunction with a JS plugin like select2 for EZ tagging interface:

```
<%= f.input :colors_list, input_html: {
        data: { tags: Topical::Tag.by('colors').collect(&:name) }
} %>
```

```
$('[data-tags]').each(function() {
  $(this).select2({
    tags: $(this).data('tags')
  });
});
```

## Testing
 Run `rake` to execute the gem's RSpec tests

You can add the tests for any host model in RSpec with the following
```
  RSpec.describe YourModel, type: :model do
    include Topical::TaggableSpec
  end
```
