module Topical
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def copy_templates
      copy_file "initializer.rb", "config/initializers/topical.rb"
    end

    def install_migrations
      rake "topical:install:migrations"
    end
  end
end