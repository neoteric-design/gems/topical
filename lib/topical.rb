# frozen_string_literal: true

require "topical/taggable"
require "topical/tag_factory"
require "topical/configuration"
require "topical/engine"
require "topical/taggable_spec"
require "topical/version"

module Topical
  mattr_reader :contexts
  @@contexts = []

  def self.register_contexts(*contexts)
    @@contexts.concat(contexts.flatten).uniq!
  end
end
