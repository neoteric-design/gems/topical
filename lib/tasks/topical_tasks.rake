desc "Copy tagging data from Acts As Taggable On"
namespace :topical do
  task migrate_aato: :environment do
    ActsAsTaggableOn::Tag.all.each do |legacy_tag|
      legacy_tag.taggings.each do |legacy_tagging|
        new_tag = Topical::Tag.where(name: legacy_tag.name, context: legacy_tagging.context).first_or_create
        new_tagging = Topical::Tagging.where(tag: new_tag, taggable: legacy_tagging.taggable).first_or_create
      end
    end
  end
end
