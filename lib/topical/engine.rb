# frozen_string_literal: true

module Topical
  class Engine < ::Rails::Engine
    isolate_namespace Topical

    config.generators do |g|
      g.test_framework :rspec
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
    end

    initializer 'topical.extend_active_record',
                :after => :load_config_initializers do
      ::ActiveSupport.on_load :active_record do
        ::ActiveRecord::Base.extend(::Topical::Taggable::ClassMethods)
      end
    end
  end
end
