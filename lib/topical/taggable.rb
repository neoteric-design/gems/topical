# frozen_string_literal: true

module Topical
  module Taggable
    module ClassMethods
      def topical_on(*contexts, **options)
        initialize_topical_for_class unless topical?

        self.topical_contexts.concat(contexts)
        Topical.register_contexts(contexts)

        contexts.each do |context|
          has_many context, -> { by(context) },
                            through: :taggings, source: :tag,
                            class_name: '::Topical::Tag', autosave: true

          define_method "#{context}_list" do
            send(context).map(&:name).join(Topical.configuration.delimiter)
          end

          define_method "#{context}_list=" do |tag_list_string|
            Topical::TagFactory.new(self, context, tag_list_string).assign
          end
        end
      end

      def tagged_with(tag)
        if self.topical?
          joins(:tags).where('topical_tags.id' => tag.id)
        end
      end

      def tagged_on(context, name)
        if self.topical?
          joins(:tags).where('topical_tags.context' => context.to_s,
                             'topical_tags.name'    => name)
        end
      end

      def topical?
        respond_to?(:topical_contexts) && topical_contexts.any?
      end

      def initialize_topical_for_class
        class_attribute :topical_contexts
        self.topical_contexts ||= []

        has_many :taggings, as: :taggable, dependent: :delete_all,
                            class_name: '::Topical::Tagging', autosave: true
        has_many :tags, through: :taggings, source: :tag,
                        class_name: '::Topical::Tag', autosave: true
      end
    end
  end
end
