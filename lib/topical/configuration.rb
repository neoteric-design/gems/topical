# frozen_string_literal: true

module Topical
  class Configuration
    attr_accessor :delimiter

    def initialize(delimiter: ',')
      @delimiter = delimiter
    end
  end

  class << self
    attr_writer :configuration
    def configuration
      @configuration ||= Configuration.new
    end

    def reset
      @configuration = Configuration.new
    end

    def configure
      yield(configuration)
    end
  end
end