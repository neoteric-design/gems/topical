# frozen_string_literal: true

module Topical
  class TagFactory
    attr_reader :object, :context, :tag_list
    def initialize(object, context, tag_list)
      @object = object
      @context = context
      @tag_list = tag_list
    end

    def assign
      attach_error && return unless tags.all?(&:valid?)

      object.send("#{context}=", tags)
      tags.each_with_index do |tag, idx|
        object.taggings.where(tag: tag).update_all(position: idx)
      end
    end

    private

    def split_tag_list
      @split_tag_list ||= @tag_list.to_s.split(Topical.configuration.delimiter)
                                   .map(&:strip).reject(&:blank?).compact.uniq
    end

    def tags
      split_tag_list.map { |tag_name| Tag.from_factory(name: tag_name, context: context) }
    end

    def attach_error
      object.errors.add("#{context}_list".to_sym, tag.errors.messages)
    end
  end
end
