# frozen_string_literal: true

module Topical
  # Taggable model lint
  module TaggableSpec
    def self.included(base)
      base.let(:taggable) { described_class.new }
      base.let(:other_taggable) { described_class.new }
      base.let!(:a_context) { described_class.topical_contexts.first }

      base.describe 'topical_on' do
        it "has contexts" do
          expect(described_class.topical?).to be_truthy
          expect(described_class.topical_contexts).to be_present
        end

        described_class.topical_contexts.each do |tag_type|
          it "should have a #{tag_type} association" do
            expected_type = ActiveRecord::Associations::CollectionProxy
            expect(taggable.send(tag_type)).to be_an(expected_type)
          end
        end
      end

      base.describe 'list retrieval' do
        it 'returns delimiter separated list' do
          taggable.save

          rabbit = Topical::Tag.create name: 'Rabbit', context: a_context.to_s
          horse  = Topical::Tag.create name: 'Horse', context: a_context.to_s
          blue   = Topical::Tag.create name: 'Blue', context: 'colors'
          taggable.tags = [rabbit, horse, blue]
          taggable.save

          expect(taggable.send("#{a_context}_list")).to eq('Rabbit,Horse')
        end
      end

      base.describe 'list assignment' do

        it 'should assign tags from string list' do
          taggable.send("#{a_context}_list=", 'Feline,Canine')
          expect(taggable.send("#{a_context}_list")).to eq('Feline,Canine')

          taggable.save
          expect(taggable.taggings.count).to eq(2)
        end

        it 'should remove prior tags without deleting the tag itself' do
          taggable.send("#{a_context}_list=", 'Feline,Turtle,Canine')
          expect(taggable.send("#{a_context}_list")).to eq('Feline,Turtle,Canine')

          taggable.save
          expect(taggable.taggings.count).to eq(3)

          taggable.send("#{a_context}_list=", 'Feline,Canine')
          expect(taggable.send("#{a_context}_list")).to eq('Feline,Canine')

          taggable.save
          expect(taggable.reload.taggings.count).to eq(2)
          expect(Topical::Tag.where(name: 'Turtle')).not_to be_empty
        end

        it 'ignores blanks' do
          taggable.send("#{a_context}_list=", 'Feline, ,Canine,,')
          taggable.save
          expect(taggable.send("#{a_context}_list")).to eq('Feline,Canine')
          expect(taggable.taggings.count).to eq(2)
        end
      end

      base.describe "deletion" do
        describe "taggable destroyed" do
          it "delete_all associated taggings" do
            taggable.send("#{a_context}_list=", 'Feline,Canine')
            other_taggable.send("#{a_context}_list=", 'Feline,Canine')
            taggable.save; other_taggable.save

            taggable.destroy

            expect(Topical::Tagging.where(taggable: taggable)).to be_empty
            expect(Topical::Tagging.where(taggable: other_taggable).count).to eq(2)
          end
        end

        describe "tag destroyed" do
          it "delete_all associated taggings" do
            taggable.send("#{a_context}_list=", 'Feline,Canine')
            other_taggable.send("#{a_context}_list=", 'Feline,Canine')
            taggable.save; other_taggable.save
            tag = Topical::Tag.by(a_context).find_by(name: 'Feline')

            tag.destroy!

            expect(Topical::Tagging.where(tag: tag)).to be_empty
            expect(taggable.reload.send("#{a_context}_list")).to eq("Canine")
            expect(other_taggable.reload.send("#{a_context}_list")).to eq("Canine")
          end
        end
      end
    end
  end
end
