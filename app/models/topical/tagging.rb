# frozen_string_literal: true

module Topical
  class Tagging < ::ActiveRecord::Base
    belongs_to :taggable, polymorphic: true, optional: true
    belongs_to :tag
    default_scope -> { order :position }
  end
end
