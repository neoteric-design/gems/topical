# frozen_string_literal: true

module Topical
  class Tag < ::ActiveRecord::Base
    has_many :taggings, dependent: :delete_all
    has_many :taggables, through: :taggings

    before_validation :set_slug
    validates_presence_of [:name, :context]
    validates_uniqueness_of [:name, :slug], scope: :context,
      message: 'Name is taken or too similar to an existing tag'

    def self.by(context)
      where(context: context.to_s)
    end

    def self.find_by_slug(context, slug)
      find_by(slug: slug, context: context)
    end

    def self.from_factory(name: '', context: '')
      if tag = where(slug: slug_template(name), context: context).first
        tag
      else
        tag = create(name: name, context: context)
      end
    end

    private
    def set_slug
      self[:slug] = self.class.slug_template(name)
    end

    def self.slug_template(tag_name)
      "#{tag_name.parameterize}"
    end
  end
end